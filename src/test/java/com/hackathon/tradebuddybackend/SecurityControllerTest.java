package com.hackathon.tradebuddybackend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackathon.tradebuddybackend.controller.SecurityController;
import com.hackathon.tradebuddybackend.model.SecurityHolding;
import com.hackathon.tradebuddybackend.service.SecurityService;
import com.hackathon.tradebuddybackend.service.TransactionServiceInterface;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.mockitoSession;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;



@WebMvcTest(SecurityController.class)
public class SecurityControllerTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SecurityService securityService;

    @Test
    public void testSecurityController() throws Exception {
        List<SecurityHolding> allSecurity = new ArrayList<>();
        SecurityHolding testSecurity = new SecurityHolding();
        testSecurity.setId("12345");
        testSecurity.setTicker("AAPL");
        testSecurity.setEntryPrice(120.0);
        testSecurity.setQuantity(16);
        testSecurity.setCurrentPrice(135.1);
        testSecurity.setTimestamp("2021-07-02 09:54:21");

        allSecurity.add(testSecurity);

        when(securityService.findAll()).thenReturn(allSecurity);

        this.mockMvc.perform(get("/api/v1/transaction"))
                .andDo(print())
                .andExpect(status().isOk());

        when(securityService.save(testSecurity)).thenReturn(testSecurity);

        this.mockMvc.perform(post("/api/v1/transaction")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testSecurity)))
                .andDo(print())
                .andExpect(status().isOk()).andReturn();

        Optional<SecurityHolding> optHolding = Optional.of(testSecurity);
        when(securityService.findById(testSecurity.getId())).thenReturn(optHolding);
        this.mockMvc.perform(get("/api/v1/transaction/" + testSecurity.getId()))
                .andDo(print())
                .andExpect(status().isOk()).andReturn();

        this.mockMvc.perform(delete("/api/v1/transaction/" + testSecurity.getId()))
                .andDo(print())
                .andExpect(status().isNoContent()).andReturn();
    }
}
