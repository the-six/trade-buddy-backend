package com.hackathon.tradebuddybackend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackathon.tradebuddybackend.controller.CashHoldingController;
import com.hackathon.tradebuddybackend.model.CashHolding;
import com.hackathon.tradebuddybackend.service.CashHoldingService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(CashHoldingController.class)
public class CashHoldingControllerTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CashHoldingService cashHoldingService;

    @Test
    public void testCashHoldingController() throws Exception {
        List<CashHolding> allCashHolding = new ArrayList<>();
        CashHolding testCashHolding = new CashHolding();
        testCashHolding.setId("33333");
        testCashHolding.setCurrency("CAD");
        testCashHolding.setBalance(120.0);
        testCashHolding.setFinancialService("RBC");

        allCashHolding.add(testCashHolding);

        when(cashHoldingService.findAll()).thenReturn(allCashHolding);

        this.mockMvc.perform(get("/api/v1/cashholding"))
                .andDo(print())
                .andExpect(status().isOk());

        when(cashHoldingService.save(testCashHolding)).thenReturn(testCashHolding);

        this.mockMvc.perform(post("/api/v1/cashholding")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testCashHolding)))
                .andDo(print())
                .andExpect(status().isOk()).andReturn();


        Optional<CashHolding> optHolding = Optional.of(testCashHolding);
        when(cashHoldingService.findById(testCashHolding.getId())).thenReturn(optHolding);
        this.mockMvc.perform(get("/api/v1/cashholding/" + testCashHolding.getId()))
                .andDo(print())
                .andExpect(status().isOk()).andReturn();

        this.mockMvc.perform(delete("/api/v1/cashholding/" + testCashHolding.getId()))
                .andDo(print())
                .andExpect(status().isNoContent()).andReturn();
    }
}
