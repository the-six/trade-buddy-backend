package com.hackathon.tradebuddybackend;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackathon.tradebuddybackend.controller.WatchListController;
import com.hackathon.tradebuddybackend.model.SecurityHolding;
import com.hackathon.tradebuddybackend.model.WatchListItem;
import com.hackathon.tradebuddybackend.service.WatchListService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(WatchListController.class)
public class WatchListControllerTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private WatchListService watchListService;

    @Test
    public void testWatchListController() throws Exception {
        List<WatchListItem> allWatchListItems = new ArrayList<>();
        WatchListItem testWatchlistItem = new WatchListItem();
        testWatchlistItem.setId("1111");
        testWatchlistItem.setTicker("AAPL");
        testWatchlistItem.setExchange("NASDAQ");
        testWatchlistItem.setName("Apple Inc.");

        allWatchListItems.add(testWatchlistItem);

        when(watchListService.findAll()).thenReturn(allWatchListItems);

        this.mockMvc.perform(get("/api/v1/watchlist"))
                .andDo(print())
                .andExpect(status().isOk());

        when(watchListService.save(testWatchlistItem)).thenReturn(testWatchlistItem);

        this.mockMvc.perform(post("/api/v1/watchlist")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(testWatchlistItem)))
                .andDo(print())
                .andExpect(status().isOk()).andReturn();

        Optional<WatchListItem> optWatchListItem = Optional.of(testWatchlistItem);
        when(watchListService.findById(testWatchlistItem.getId())).thenReturn(optWatchListItem);

        this.mockMvc.perform(delete("/api/v1/watchlist/" + testWatchlistItem.getId()))
                .andDo(print())
                .andExpect(status().isNoContent()).andReturn();
    }
}
