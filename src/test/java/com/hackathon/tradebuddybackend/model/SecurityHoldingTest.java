package com.hackathon.tradebuddybackend.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class SecurityHoldingTest {

        @Test
        public void testSecurityGettersSetters() {
            SecurityHolding testSecurityHolding = new SecurityHolding();
            testSecurityHolding.setId("12345");
            testSecurityHolding.setTicker("AAPL");
            testSecurityHolding.setEntryPrice(120.0);
            testSecurityHolding.setQuantity(16);
            testSecurityHolding.setCurrentPrice(135.1);
            testSecurityHolding.setTimestamp("2021-07-02 09:54:21");

            assertEquals("12345", testSecurityHolding.getId());
            assertEquals("AAPL", testSecurityHolding.getTicker());
            assertEquals(120.0, testSecurityHolding.getEntryPrice());
            assertEquals(16, testSecurityHolding.getQuantity());
            assertEquals(135.1, testSecurityHolding.getCurrentPrice());
            assertEquals("2021-07-02 09:54:21", testSecurityHolding.getTimestamp());

            System.out.println("TEST PASSED: testSecurityGettersSetters");
        }

}
