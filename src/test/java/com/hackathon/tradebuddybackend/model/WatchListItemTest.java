package com.hackathon.tradebuddybackend.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class WatchListItemTest {

        @Test
        public void testWatchlistGettersSetters() {
            WatchListItem testWatchList = new WatchListItem();
            testWatchList.setId("1111");
            testWatchList.setTicker("AAPL");
            testWatchList.setExchange("NASDAQ");
            testWatchList.setName("Apple Inc.");

            assertEquals("1111", testWatchList.getId());
            assertEquals("AAPL", testWatchList.getTicker());
            assertEquals("NASDAQ", testWatchList.getExchange());
            assertEquals("Apple Inc.", testWatchList.getName());

            System.out.println("TEST PASSED: testWatchlistGettersSetters");
        }

}
