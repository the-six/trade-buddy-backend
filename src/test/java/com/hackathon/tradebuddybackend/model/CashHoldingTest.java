package com.hackathon.tradebuddybackend.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CashHoldingTest {

        @Test
        public void testCashHoldingGettersSetters() {
            CashHolding testCash = new CashHolding();
            testCash.setId("12345");
            testCash.setCurrency("CAD");
            testCash.setBalance(120.0);
            testCash.setFinancialService("RBC");

            assertEquals("12345", testCash.getId());
            assertEquals("CAD", testCash.getCurrency());
            assertEquals(120.0, testCash.getBalance());
            assertEquals("RBC", testCash.getFinancialService());

            System.out.println("TEST PASSED: testCashHoldingGettersSetters");
        }

}
