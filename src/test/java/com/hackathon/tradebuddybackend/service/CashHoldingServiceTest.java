package com.hackathon.tradebuddybackend.service;

import com.hackathon.tradebuddybackend.model.CashHolding;
import com.hackathon.tradebuddybackend.repository.CashHoldingRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class CashHoldingServiceTest {

    @Autowired
    private CashHoldingService cashHoldingService;

    @MockBean
    private CashHoldingRepository cashHoldingRepository;

    @Test
    public void testCashHoldingService() {
        List<CashHolding> allCashHolding = new ArrayList<>();
        CashHolding testCashHolding = new CashHolding();
        testCashHolding.setId("4444");
        allCashHolding.add(testCashHolding);

        when(cashHoldingRepository.findAll()).thenReturn(allCashHolding);
        assertEquals(1, cashHoldingService.findAll().size());

        when(cashHoldingRepository.save(testCashHolding)).thenReturn(testCashHolding);
        assertEquals(testCashHolding.getId(), cashHoldingService.save(testCashHolding).getId());

        Optional<CashHolding> optHolding = Optional.of(testCashHolding);
        when(cashHoldingRepository.findById(testCashHolding.getId())).thenReturn(optHolding);
        assertEquals(optHolding, cashHoldingService.findById(testCashHolding.getId()));

        System.out.println("TEST PASSED: testCashHoldingService");
    }
}
