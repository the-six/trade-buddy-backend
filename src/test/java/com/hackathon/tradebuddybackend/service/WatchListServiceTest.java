package com.hackathon.tradebuddybackend.service;

import com.hackathon.tradebuddybackend.model.WatchListItem;
import com.hackathon.tradebuddybackend.repository.WatchListRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class WatchListServiceTest {

    @Autowired
    private WatchListService watchListService;

    @MockBean
    private WatchListRepository watchListRepository;

    @Test
    public void testWatchListService() {
        List<WatchListItem> allWatchListItem = new ArrayList<>();
        WatchListItem testWatchListItem = new WatchListItem();
        testWatchListItem.setId("5555");
        allWatchListItem.add(testWatchListItem);

        when(watchListRepository.findAll()).thenReturn(allWatchListItem);
        assertEquals(1, watchListService.findAll().size());

        when(watchListRepository.save(testWatchListItem)).thenReturn(testWatchListItem);
        assertEquals(testWatchListItem.getId(), watchListService.save(testWatchListItem).getId());

        Optional<WatchListItem> optHolding = Optional.of(testWatchListItem);
        when(watchListRepository.findById(testWatchListItem.getId())).thenReturn(optHolding);
        assertEquals(optHolding, watchListService.findById(testWatchListItem.getId()));

        System.out.println("TEST PASSED: testWatchListService");
    }
}
