package com.hackathon.tradebuddybackend.service;

import com.hackathon.tradebuddybackend.model.SecurityHolding;
import com.hackathon.tradebuddybackend.repository.SecurityRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class SecurityServiceTest {

    @Autowired
    private SecurityService securityService;

    @MockBean
    private SecurityRepository securityRepository;

    @Test
    public void testSecurityService() {
        List<SecurityHolding> allSecurity = new ArrayList<>();
        SecurityHolding testSecurity = new SecurityHolding();
        testSecurity.setId("5555");
        allSecurity.add(testSecurity);

        when(securityRepository.findAll()).thenReturn(allSecurity);
        assertEquals(1, securityService.findAll().size());

        when(securityRepository.save(testSecurity)).thenReturn(testSecurity);
        assertEquals(testSecurity.getId(), securityService.save(testSecurity).getId());

        Optional<SecurityHolding> optHolding = Optional.of(testSecurity);
        when(securityRepository.findById(testSecurity.getId())).thenReturn(optHolding);
        assertEquals(optHolding, securityService.findById(testSecurity.getId()));

        System.out.println("TEST PASSED: testSecurityService");
    }
}
