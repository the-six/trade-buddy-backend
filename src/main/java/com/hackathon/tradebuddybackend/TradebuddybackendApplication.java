package com.hackathon.tradebuddybackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TradebuddybackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(TradebuddybackendApplication.class, args);
    }

}
