package com.hackathon.tradebuddybackend.repository;

import com.hackathon.tradebuddybackend.model.SecurityHolding;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface SecurityRepository extends MongoRepository<SecurityHolding, String> {
}
