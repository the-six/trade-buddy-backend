package com.hackathon.tradebuddybackend.repository;

import com.hackathon.tradebuddybackend.model.CashHolding;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface CashHoldingRepository extends MongoRepository<CashHolding, String> {
}
