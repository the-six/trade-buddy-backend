package com.hackathon.tradebuddybackend.repository;

import com.hackathon.tradebuddybackend.model.WatchListItem;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface WatchListRepository extends MongoRepository<WatchListItem, String> {
}
