package com.hackathon.tradebuddybackend.controller;

import com.hackathon.tradebuddybackend.model.SecurityHolding;
import com.hackathon.tradebuddybackend.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/transaction")
public class SecurityController {
    @Autowired
    private SecurityService transactionService;

    /**
     * Returns all Security Holdings.
     */
    @GetMapping
    public List<SecurityHolding> findAll() {
        return transactionService.findAll();
    }

    /**
     * Attempts to find and return a Security Holding based on id.
     */
    @GetMapping("{id}")
    public ResponseEntity<SecurityHolding> findById(@PathVariable String id) {
        try {
            return new ResponseEntity<SecurityHolding>(transactionService.findById(id).get(), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Saves a new Security Holding into the database.
     */
    @PostMapping
    public SecurityHolding save(@RequestBody SecurityHolding securityHolding) {
        return transactionService.save(securityHolding);
    }

    /**
     * Attempts to find and delete a Security Holding based on id.
     */
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable String id) {
        if(transactionService.findById(id).isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        transactionService.delete(id);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
