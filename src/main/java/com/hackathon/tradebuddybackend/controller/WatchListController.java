package com.hackathon.tradebuddybackend.controller;

import com.hackathon.tradebuddybackend.model.SecurityHolding;
import com.hackathon.tradebuddybackend.model.WatchListItem;
import com.hackathon.tradebuddybackend.service.WatchListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/watchlist")
public class WatchListController {
    @Autowired
    private WatchListService watchListService;

    /**
     * Returns all Watchlist items.
     */
    @GetMapping
    public List<WatchListItem> findAll() {
        return watchListService.findAll();
    }


    /**
     * Saves a new Watchlist item into the database.
     */
    @PostMapping
    public WatchListItem save(@RequestBody WatchListItem watchListItem) {
        return watchListService.save(watchListItem);
    }

    /**
     * Attempts to find and delete a Watchlist item based on id.
     */
    @DeleteMapping("{id}")
    public ResponseEntity<WatchListItem> delete(@PathVariable String id) {
        if(watchListService.findById(id).isEmpty()) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

        watchListService.delete(id);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
