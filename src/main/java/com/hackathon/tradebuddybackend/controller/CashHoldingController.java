package com.hackathon.tradebuddybackend.controller;

import com.hackathon.tradebuddybackend.model.CashHolding;
import com.hackathon.tradebuddybackend.service.CashHoldingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/cashholding")
public class CashHoldingController {
    @Autowired
    private CashHoldingService transactionService;

    /**
     * Returns all Cash Holdings.
     */
    @GetMapping
    public List<CashHolding> findAll() {
        return transactionService.findAll();
    }

    /**
     * Attempts to find and return a Cash Holding based on id.
     */
    @GetMapping("{id}")
    public ResponseEntity<CashHolding> findById(@PathVariable String id) {
        try {
            return new ResponseEntity<CashHolding>(transactionService.findById(id).get(), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    /**
     * Saves a new Cash Holding into the database.
     */
    @PostMapping
    public CashHolding save(@RequestBody CashHolding cashHolding) {
        return transactionService.save(cashHolding);
    }

    /**
     * Attempts to find and delete a Cash Holding based on a given id.
     */
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable String id) {
        if(transactionService.findById(id).isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        transactionService.delete(id);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
