package com.hackathon.tradebuddybackend.service;

import com.hackathon.tradebuddybackend.model.CashHolding;
import com.hackathon.tradebuddybackend.repository.CashHoldingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class CashHoldingService implements TransactionServiceInterface<CashHolding> {

    @Autowired
    private CashHoldingRepository cashHoldingRepository;

    @Override
    public List<CashHolding> findAll() {
        return cashHoldingRepository.findAll();
    }

    @Override
    public CashHolding save(CashHolding cashHolding) {
        return cashHoldingRepository.save(cashHolding);
    }

    @Override
    public Optional<CashHolding> findById(String id) {
        return cashHoldingRepository.findById(id);
    }

    @Override
    public void delete(String id) {
        cashHoldingRepository.deleteById(id);
    }


}
