package com.hackathon.tradebuddybackend.service;

import java.util.List;
import java.util.Optional;

public interface TransactionServiceInterface<T> {

    /**
     * Finds and returns all Transaction items of type T from repository.
     */
    List<T> findAll();

    /**
     * Saves a Transaction item of type T to repository.
     */
    T save(T t);

    /**
     * Attempts to find and return a Transaction item of type T by a given id.
     */
    Optional<T> findById(String id);

    /**
     * Attempts to delete a Transaction item of type T based on the given id.
     */
    void delete(String id);
}
