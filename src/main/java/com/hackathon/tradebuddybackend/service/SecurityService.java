package com.hackathon.tradebuddybackend.service;

import com.hackathon.tradebuddybackend.model.SecurityHolding;
import com.hackathon.tradebuddybackend.repository.SecurityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class SecurityService implements TransactionServiceInterface<SecurityHolding> {

    @Autowired
    private SecurityRepository securityRepository;

    @Override
    public List<SecurityHolding> findAll() {
        return securityRepository.findAll();
    }

    @Override
    public SecurityHolding save(SecurityHolding securityHolding) {
        return securityRepository.save(securityHolding);
    }

    @Override
    public Optional<SecurityHolding> findById(String id) {
        return securityRepository.findById(id);
    }

    @Override
    public void delete(String id) {
        securityRepository.deleteById(id);
    }

}
