package com.hackathon.tradebuddybackend.service;

import com.hackathon.tradebuddybackend.model.WatchListItem;
import com.hackathon.tradebuddybackend.repository.WatchListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class WatchListService {

    @Autowired
    private WatchListRepository watchlistRepository;

    /**
     * Finds and returns all Watchlist items from repository.
     */
    public List<WatchListItem> findAll() {
        return watchlistRepository.findAll();
    }

    /**
     * Saves a Watchlist item to repository.
     */
    public WatchListItem save(WatchListItem watchListItem) {
        return watchlistRepository.save(watchListItem);
    }

    /**
     * Attempts to find and return a Watchlist item by a given id.
     */
    public Optional<WatchListItem> findById(String id) {
        return watchlistRepository.findById(id);
    }

    /**
     * Attempts to delete a Watchlist item based on the given id.
     */
    public void delete(String id) {
        watchlistRepository.deleteById(id);
    }

}
