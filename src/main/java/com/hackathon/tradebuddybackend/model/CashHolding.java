package com.hackathon.tradebuddybackend.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class CashHolding {

    @Id
    private String id;
    private String financialService;
    private String currency;
    private double balance;

}
