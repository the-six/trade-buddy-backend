package com.hackathon.tradebuddybackend.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class SecurityHolding {

    @Id
    private String id;
    private String ticker;
    private double entryPrice;
    private int quantity;
    private double currentPrice;
    private String timestamp;

}
