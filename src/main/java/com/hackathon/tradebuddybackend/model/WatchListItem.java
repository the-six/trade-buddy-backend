package com.hackathon.tradebuddybackend.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class WatchListItem {
    @Id
    private String id;
    private String ticker;
    private String name;
    private String exchange;
}
